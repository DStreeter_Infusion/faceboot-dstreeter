require('./assets/styles/index.css');
require('./assets/styles/main.css');
require('./assets/styles/custom.css');

require('angular');
require('angular-ui-router');

angular.module('app', ['ui.router'])

.config(function($stateProvider,$locationProvider,$urlRouterProvider){
	$locationProvider.html5Mode(true); // getting rid of #
	$urlRouterProvider.otherwise('/'); // if unknown -> got

	$stateProvider

	.state("root", {
		url: '/', // what the url should look like
		component: 'container'
	})
});

//require("./components/root.component.js");
//require("./components/child.component.js");

const contextController = require.context('.', true, /\.controller\.js$/);
contextController.keys().forEach(contextController);

const context = require.context('.', true, /\.component\.js$/);
context.keys().forEach(context);

console.dir(angular);