angular.module('app')

.component('post', {
	controller: 'postCtrl',
	bindings: {
		data: '<'
	},
	template: 	`<div class="post">
		            <!--USER POST HEADER-->
		            <div class="post__header flex-between margin-16">
		                <a class="profile profile--lg profile--hover-ul" href="#">
		                    <img src="./assets/stock_profile.jpg">
		                    <span>{{$ctrl.data.username}}</span>
		                </a>
		                <span class="timestamp">{{$ctrl.data.published_at}}</span>
		            </div>
		            <!--USER POST CONTENT-->
		            <div class="text-md margin-16">
		                {{$ctrl.data.content}}
		            </div>
		            <!--USER POST FOOTER-->
		            <div class="flex-between margin-16">
		                <div class="flex">
		                    <a class="btn-icon btn--hover-red btn-like" ng-click='addLike($ctrl.data, $event)' href="#"><i class="fa fa-heart fa-lg"></i></a>
		                    <span class="span-like">{{$ctrl.data.likes}}</span>
		                </div>
		                <div class="flex">
		                    <a class="btn-icon comment-count" href="#"><i class="fa fa-comments-o fa-lg"></i></a>
		                    <span>{{$ctrl.data.replies.length}}</span>
		                </div>
		            </div>
		            <!--USER POST COMMENTS-->
		            <ul class="margin-16 top-divider">
		                <div class="comments">
		                    <!--USER COMMENT-->
		                    <li class="flex-col" ng-repeat="comment in $ctrl.data.replies">
		                        <!--USER COMMENT HEADER-->
		                        <div class="flex-between">
		                            <a class="profile profile--hover-ul" href="#">
		                                <img src="./assets/stock_profile.jpg">
		                                <span>{{comment.username}}</span>
		                            </a>
		                            <span class="timestamp text-sm">{{comment.published_at}}</span>
		                        </div>
		                        <!--USER COMMENT CONTENT-->
		                        <p class="text-md margin-8-top">
		                            {{comment.content}}
		                        </p>
		                    </li>

		                </div>
		                <!-- POST INPUT COMPONENT-->
		                <div class="p-input">
		                    <div contentEditable="true" class="p-input__ta" placeholder="Add a comment..."></div>
		                    <a class="btn btn-primary f-right margin-8-v" ng-href="#here" ng-click='addComment($ctrl.data, $event)'>POST</a>
		                </div>
		            </ul>
		        </div>`
});