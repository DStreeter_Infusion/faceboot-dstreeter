angular.module('app')

.component('container', {
	controller: 'containerCtrl',
	bindings: {
		item: '<'
	},
	template: 	`
				<header class="page-header">
				    <!--navbar block-->
				    <div class="navbar">
				        <a class="logo" href="https://www.facebook.com"></a>
				        <ul class="nav-h">
				            <li>
				                <a class="btn profile profile--hover-bg">
				                    <img src="./assets/profile.jpg">
				                    <span>Username</span>
				                </a>
				            </li>
				            <li>
				                <a class="btn" href="#" id="logout">
				                    <i class="fa fa-sign-out" aria-hidden="true"></i>
				                    <span>Log Out</span>
				                </a>
				            </li>
				        </ul>
				    </div>
				</header>
				<div class="container">
					<div class="content">
				  		<modal></modal>
						<post data="post" ng-repeat="post in posts"></post>
					</div>
					<div class="aside">
						<user data="user" ng-repeat="user in users"></user>
					</div>
				</div>`
})

.component('user', {
	bindings: {
		data: '<'
	},
	template: 	`
				<a class="btn profile profile--hover-bg">
                    <img src="./assets/profile.jpg">
                    <span>{{$ctrl.data.username}}</span>
                </a>
				`
});