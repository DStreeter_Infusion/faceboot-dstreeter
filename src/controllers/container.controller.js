angular.module('app')

.service('PostService', function($http) {
	this.getAllPosts = function() {
		var promise = 	$http({
					        method : 'GET',
					        url : 'http://private-36ae7-bootcamp3.apiary-mock.com/questions'
					    }).then(function(response) {
							return response.data;
						}, function(response) {
							return response.statusText;
						});
		return promise;
	};

	this.submitPost = function(username, content) {
		$http({
			method: 'POST',
			url: 'http://private-36ae7-bootcamp3.apiary-mock.com/questions',
			headers: {
				'Content-Type': 'application/json'
			},
			data: { 
				'username': "Test",
				'content': content
			}
		}).then(function(response) {
			this.getAllPosts();
			console.dir(response.data);
		}, function(response) {
			console.dir(response.statusText);
		});
	};

	this.getAllUsers = function() {
		var promise = 	$http({
					        method : 'GET',
					        url : 'http://private-36ae7-bootcamp3.apiary-mock.com/users'
					    }).then(function(response) {
							return response.data;
						}, function(response) {
							return response.statusText;
						});
		return promise;
	};
})

.controller('containerCtrl', function($scope, $http, PostService) {
	
	$scope.getAllPosts = function() {
		PostService.getAllPosts().then(function(posts) {
			$scope.posts = posts;
		});
	}

	$scope.getAllUsers = function() {
		PostService.getAllUsers().then(function(users) {
			$scope.users = users;
		});
	}

	$scope.getAllPosts();
	$scope.getAllUsers();
});