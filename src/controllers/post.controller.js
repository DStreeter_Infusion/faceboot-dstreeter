angular.module('app')

.service('')

.controller('postCtrl', function($scope, $http) {
	$scope.addComment = function(post, $event) {
		post.replies.push({"username": $event.currentTarget.ownerDocument.getElementsByClassName("profile")[0].childNodes[3].innerText, "published_at": new Date(), "content": $event.currentTarget.parentNode.getElementsByClassName("p-input__ta")[0].innerText});
		$event.currentTarget.parentNode.getElementsByClassName("p-input__ta")[0].innerText = "";
	}

	$scope.addLike = function(post, $event) {
		var likeButton = $event.currentTarget;
		likeButton.classList.toggle("btn--hover-red");
		likeButton.classList.toggle("btn--active");
		if (likeButton.classList.contains("btn--hover-red"))
			post.likes--; // decrement likes
		if (likeButton.classList.contains("btn--active"))
			post.likes++; // increment likes
	}
});